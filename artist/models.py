from django.db import models
from django.utils import timezone


class Type(models.Model):
    label = models.CharField(max_length=30)

    def __str__(self):
        return "{0} {1}".format(self.id, self.label)


class Artist(models.Model):
    name = models.CharField(max_length=25)
    firstName = models.CharField(max_length=15)
    birthday = models.DateField(default=timezone.now)
    biography = models.TextField(max_length=1000)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return "{0} {1} {2} {3} {4}"\
            .format(self.id, self.name, self.firstName, self.birthday, self.biography)


class Creation(models.Model):
    title = models.CharField(max_length=100)
    date = models.DateField(
        verbose_name='Creation date',
        default=timezone.now
    )
    place = models.CharField(max_length=100)
    content = models.TextField(max_length=1000)
    type = models.ForeignKey(Type, null=True, on_delete=models.SET_NULL)
    artist = models.ForeignKey(Artist, null=False, on_delete=models.CASCADE)
    # TODO: Put images

    class Meta:
        ordering = ['date']

    def __str__(self):
        return "{0} {1} {2} {3} {4} {5}"\
            .format(self.id, self.title, self.date, self.place, self.content, self.type)

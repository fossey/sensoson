from django.test import TestCase
from artist.models import Artist, Creation, Type
from datetime import date


class ArtistTests(TestCase):

    def setUp(self):
        self.default_type = Type.objects.get_or_create(label="Setup")[0]

        self.artist = Artist.objects.get_or_create(
            name="Bureau",
            firstName="Victor",
            birthday=date(1994, 1, 14),
            biography="Bio"
        )[0]

        self.creation = Creation.objects.get_or_create(
            title="JLAC",
            date=date.today(),
            place="Caen",
            content="Lorem Ipsum",
            type_id=self.default_type.id,
            artist=self.artist
        )[0]

    def test_artist_creation(self):
        """
        Check creation Artist, Creation and Category
        """

        self.assertEqual(self.default_type.id, 1)
        self.assertEqual(self.creation.id, 1)
        self.assertEqual(self.artist.id, 1)

        self.assertEqual(self.artist.creation_set.count(), 1)

    def test_check_constraints_when_creation_is_deleted(self):

        self.assertEqual(self.artist.creation_set.count(), 1)

        c1 = Creation.objects.get(title="JLAC")
        c1.delete()

        self.assertEqual(self.artist.creation_set.count(), 0)

    def test_check_constraints_when_artist_is_deleted(self):

        self.assertEqual(Creation.objects.all().count(), 1)

        a = Artist.objects.get(name="Bureau")
        a.delete()

        self.assertEqual(Creation.objects.all().count(), 0)
        self.assertEqual(Artist.objects.all().count(), 0)
        self.assertEqual(Type.objects.all().count(), 1)

from django.urls import path
from . import views


app_name = 'artists'

urlpatterns = [
    path('', views.show_artists, name='index'),
    path('<int:artist_id>', views.show_artist, name='by_id'),
    path('<int:artist_id>/creations/<int:creation_id>', views.get_artist_creation, name="by_artist_and_creation_id")
]

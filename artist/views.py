from django.shortcuts import render, get_object_or_404
from artist.models import Artist, Creation


def show_artists(request):
    return render(request, 'artists.html', {'artists': Artist.objects.all()})


def show_artist(request, artist_id):
    artist = get_object_or_404(Artist, id=artist_id)
    return render(request, 'artist.html', {'artist': artist})


def get_artist_creation(request, artist_id, creation_id):
    artist = get_object_or_404(Artist, id=artist_id)
    creation = get_object_or_404(Creation, id=creation_id)
    return render(request, 'creation.html', {'artist': artist, 'creation': creation})

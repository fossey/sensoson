from django.shortcuts import render
from artist.models import Artist


def home(request):
    return render(request, 'home.html', {'artists': Artist.objects.all()})
